function scanOp(op) {
  let elems = [];
  let buffer = "";

  for (let chr of op) {
    if (!isNaN(chr) || chr == ".") {
      buffer += chr;
      continue;
    }

    if (buffer != "") {
      elems.push(buffer);
      buffer = "";
    }

    elems.push(chr);
  }

  if (buffer != "") {
    elems.push(buffer);
  }

  return elems;
}
