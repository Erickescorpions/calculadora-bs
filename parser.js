function parseConstant(op) {
  let currentValue = op.shift();
  if(!isNaN(currentValue) && currentValue != undefined) {
    return {
      "node_name": "constant",
      "value": currentValue,
      "left_node": null,
      "right_node": null
    };
  }

  return null;
}

function parseMulDiv(op) {
  let exp = parseConstant(op);

  if(exp) {
    let currentValue = op[0];
    while(currentValue != undefined && (currentValue == "x" || currentValue == "/")) {
      op.shift(); 

      let nextExp = parseConstant(op);

      if(!nextExp) return null;

      exp =  {
        "node_name": "arith_op",
        "value": currentValue,
        "left_node": exp,
        "right_node": nextExp
      };

      currentValue = op[0];
    }

    return exp;
  }

  return null;
}

function parseOp(op) {  
  let exp = parseMulDiv(op)

  if(exp) {
    let currentValue = op[0];

    while(currentValue != undefined && (currentValue == "+" || currentValue == "-")) {
      op.shift();
      
      let nextExp = parseMulDiv(op);

      if(!nextExp) return null;

      exp =  {
        "node_name": "arith_op",
        "value": currentValue,
        "left_node": exp,
        "right_node": nextExp
      };

      currentValue = op[0];
    }

    return exp;
  }

  return null;
}