const input = document.querySelector("#input-screen");
const output = document.querySelector("#output-screen");
const buttons = document.querySelectorAll("button");
let ans = 0;

function evalOperation(op) {
  let sanitizeOutput = sanitizeSource(op, ans);
  let lexerOutput = scanOp(sanitizeOutput);
  let parserOutput = parseOp(lexerOutput);
  let result = opGenerator(parserOutput);

  return result;
}

function updateScreenInput(value) {
  if(output.innerHTML.trim() == "_") {
    let inputScreenValue = input.innerHTML.trim();
  
    if (inputScreenValue == "|") {
      inputScreenValue = "";
    }
  
    if (value == "=") {
      let res = evalOperation(input.innerHTML.trim());
      output.innerHTML = res;
      ans = res;
      return;
    }

    if(value == "Del") {
      let inputVal = input.innerHTML.trim();
      let inputSize = inputVal.length;
      if(inputSize == 1) {
        input.innerHTML = "|";
      } else {
        input.innerHTML = inputVal.slice(0, inputSize - 1);
      }
      return;
    }
  
    input.innerHTML = inputScreenValue + value;
  } else {
    if (!isNaN(value) || value == ".") {
      output.innerHTML = " _ ";
      input.innerHTML = value;
    } else if(value == "AC") {
      output.innerHTML = " _ ";
      input.innerHTML = "|"
    } else if(value != "Del") {
      output.innerHTML = " _ ";
      input.innerHTML = "Ans" + value;
    }
  }
}

buttons.forEach((button) => {
  button.addEventListener("click", () => {
    updateScreenInput(button.innerHTML.trim());
  });
});