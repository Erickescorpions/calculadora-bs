function doOperation(op, val1, val2) {
  if(!val1 && !val2) {
    return parseFloat(op);
  }

  switch(op) {
    case "+":
      return val1 + val2
    case "-":
      return val1 - val2
    case "x":
      return val1 * val2
    case "/":
      return val1 / val2
  }
}

function postOrder(node) {
  if(!node) return null;

  let eval_l = postOrder(node.left_node);
  let eval_r = postOrder(node.right_node);

  return doOperation(node.value, eval_l, eval_r);
}

function opGenerator(ast) {
  return postOrder(ast);
}